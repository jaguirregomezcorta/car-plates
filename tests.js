test("Invalid characters", function (assert){
  assert.equal(isValidPlate("1234","ABC"), false, "LETTER 'A'");
  assert.equal(isValidPlate("1234","EBC"), false, "LETTER 'E'");
  assert.equal(isValidPlate("1234","IBC"), false, "LETTER 'I'");
  assert.equal(isValidPlate("1234","OBC"), false, "LETTER 'O'");
  assert.equal(isValidPlate("1234","UBC"), false, "LETTER 'U'");
  assert.equal(isValidPlate("1234","ÑBC"), false, "LETTER 'Ñ'");
  assert.equal(isValidPlate("1234","QBC"), false, "LETTER 'Q'");
});

test("Lowercase characters", function (assert){
  assert.equal(isValidPlate("1234","bCD"), false, "LOWERCASE CHARACTER IN FIRST POSITION");
  assert.equal(isValidPlate("1234","BcD"), false, "LOWERCASE CHARACTER IN SECOND POSITION");
  assert.equal(isValidPlate("1234","bcD"), false, "LOWERCASE CHARACTER IN FIRST AND SECOND POSITIONS");
  assert.equal(isValidPlate("1234","BCd"), false, "LOWERCASE CHARACTER IN THIRD POSITION");
  assert.equal(isValidPlate("1234","bCd"), false, "LOWERCASE CHARACTER IN FIRST AND THIRD POSITIONS");
  assert.equal(isValidPlate("1234","Bcd"), false, "LOWERCASE CHARACTER IN SECOND AND THIRD POSITIONS");
  assert.equal(isValidPlate("1234","bcd"), false, "LOWERCASE CHARACTER IN ALL POSITIONS");
});

test("Mixed positions", function (assert){
  assert.equal(isValidPlate("BCD1","234"), false, "INVERTED ORDER");
  assert.equal(isValidPlate("1B2C","3D4"), false, "MIXED NUMBERS AND CHARACTERS");
});

test("Special characters", function (assert){
  assert.equal(isValidPlate("!234","BCD"), false, "SPECIAL CHARACTER '!' IN FIRST POSITION");
  assert.equal(isValidPlate("1!34","BCD"), false, "SPECIAL CHARACTER '!' IN SECOND POSITION");
  assert.equal(isValidPlate("12!4","BCD"), false, "SPECIAL CHARACTER '!' IN THIRD POSITION");
  assert.equal(isValidPlate("123!","BCD"), false, "SPECIAL CHARACTER '!' IN FOURTH POSITION");
  assert.equal(isValidPlate("1234","CD"), false, "SPECIAL CHARACTER '!' IN FIFTH POSITION");
  assert.equal(isValidPlate("1234","B!D"), false, "SPECIAL CHARACTER '!' IN SIXTH POSITION");
  assert.equal(isValidPlate("1234","BC!"), false, "SPECIAL CHARACTER '!' IN SEVENTH POSITION");

  assert.equal(isValidPlate(" 1234","BCD"), false, "SPACE CHARACTER BEFORE FIRST CHARACTER");
  assert.equal(isValidPlate("1 234","BCD"), false, "SPACE CHARACTER BETWEEN FIRST AND SECOND POSITIONS");
  assert.equal(isValidPlate("12 34","BCD"), false, "SPACE CHARACTER BETWEEN SECOND AND THIRD POSITIONS");
  assert.equal(isValidPlate("123 4","BCD"), false, "SPACE CHARACTER BETWEEN THIRD AND FOURTH POSITIONS");
  assert.equal(isValidPlate("1234 ","BCD"), false, "SPACE CHARACTER BETWEEN FOURTH AND FIFTH POSITIONS");
  assert.equal(isValidPlate("1234","B CD"), false, "SPACE CHARACTER BETWEEN FIFTH AND SIXTH POSITIONS");
  assert.equal(isValidPlate("1234","BC D"), false, "SPACE CHARACTER BETWEEN SIXTH AND SEVENTH POSITIONS");
  assert.equal(isValidPlate("1234","BCD "), false, "SPACE CHARACTER BETWEEN AFTER SEVENTH CHARACTER");
});

test("Size boundaries", function (assert){
  assert.equal(isValidPlate("1",""), false, "ONE NUMBER");
  assert.equal(isValidPlate("","B"), false, "ONE CHARACTER");
  assert.equal(isValidPlate("1","B"), false, "ONE NUMBER - ONE CHARACTER");
  assert.equal(isValidPlate("12","B"), false, "TWO NUMBERS - ONE CHARACTER");
  assert.equal(isValidPlate("12","BC"), false, "TWO NUMBERS - TWO CHARACTERS");
  assert.equal(isValidPlate("123","BC"), false, "THREE NUMBERS - TWO CHARACTERS");
  assert.equal(isValidPlate("123","BCD"), false, "THREE NUMBERS - THREE CHARACTERS");
  assert.equal(isValidPlate("1234","BCD"), true, "FOUR NUMBERS - THREE CHARACTERS");
  assert.equal(isValidPlate("1234","BCDF"), false, "FOUR NUMBERS - FOUR CHARACTERS");
  assert.equal(isValidPlate("12345","BCD"), false, "FIVE NUMBERS - THREE CHARACTERS");
  assert.equal(isValidPlate("12345","BCDF"), false, "FIVE NUMBERS - FOUR CHARACTERS");
  assert.equal(isValidPlate("12345B","CDFG"), false, "FIVE NUMBERS - FIVE CHARACTERS");
});

test("Valid plates", function (assert){
  assert.equal(isValidPlate("1235","BFJ"), true, "VALID PLATE 1");
  assert.equal(isValidPlate("6554","KDH"), true, "VALID PLATE 2");
  assert.equal(isValidPlate("8496","GYV"), true, "VALID PLATE 3");
  assert.equal(isValidPlate("2316","CDF"), true, "VALID PLATE 4");
  assert.equal(isValidPlate("4869","JDP"), true, "VALID PLATE 5");
  assert.equal(isValidPlate("6518","VDM"), true, "VALID PLATE 6");
  assert.equal(isValidPlate("5648","VJN"), true, "VALID PLATE 7");
  assert.equal(isValidPlate("6226","WCD"), true, "VALID PLATE 8");
  assert.equal(isValidPlate("8795","KJB"), true, "VALID PLATE 9");
});