/*
 * Given a plate check whether or not it is valid.
 */
function isValidPlate(number, letters) { // eslint-disable-line no-unused-vars

  if ((number.length == 4 ) && (letters.length == 3)) {
    var plate = number + letters;
    var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/; //i
    if(plate.match(re) !== null) {
      gtag('event', 'click', { // eslint-disable-line
        'event_category': 'Validate',
        'event_label': 'Valid Car Plate'
      });
      return true;
    }
    gtag('event', 'click', { // eslint-disable-line
      'event_category': 'Validate',
      'event_label': 'Not a Valid Car Plate'
    });
    return false;
  } else {
    gtag('event', 'click', { // eslint-disable-line
      'event_category': 'Validate',
      'event_label': 'Incorrect Plate Dimensions'
    });
    return false;
  }	
}